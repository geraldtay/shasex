/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thotheolh.crypto.protocol.shasex;

import java.security.SecureRandom;
import org.thotheolh.crypto.cipher.CipherAdapter;

/**
 *
 * @author gerald
 */
public class Responder {

    CipherAdapter cipher;
    byte[] secret;
    int keyLen = 32; // 256 bit key length as default.
    byte[] U;
    SecureRandom srand = new SecureRandom();

    public Responder(CipherAdapter cipher, byte[] secret, int keyByteLength) {
        this.cipher = cipher;
        this.secret = secret;
        if (keyByteLength >= 16) {
            this.keyLen = keyByteLength;
        }
    }

    // To be called first. Produces encrypted session key.
    public byte[] acceptRequest(byte[] incomingR) {
        byte[] R = cipher.decryptBlock(secret, incomingR);
        System.out.println("Resp - Accept :: Decrypt :: EK(R) (" + R.length + "), R: " + Utils.bytesToHex(R));
        U = srand.generateSeed(keyLen);
        System.out.println("U (" + U.length + "), " + Utils.bytesToHex(U));
        return cipher.encryptBlock(R, U);
    }
    
    public byte[] getSessionKey() {
        return U;
    }

}
