/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thotheolh.crypto.protocol.shasex.test;

import org.thotheolh.crypto.protocol.shasex.Requester;
import org.thotheolh.crypto.protocol.shasex.Responder;
import org.thotheolh.crypto.protocol.shasex.Utils;

/**
 *
 * @author gerald
 */
public class SHASEXTest {

    public static void main(String[] args) {
        String sharedsecret = "kWmHe8xIsDpfzK4d";
        AESAdapter cipher = new AESAdapter();
        Requester req = new Requester(cipher, strToBytes(sharedsecret), 16);
        Responder resp = new Responder(cipher, strToBytes(sharedsecret), 16);
        byte[] R = req.beginRequest();
        System.out.println("Req - Begin :: Encrypt :: EK(R): " + Utils.bytesToHex(R));
        byte[] U = resp.acceptRequest(R);
        System.out.println("Resp - Accept :: Encrypt :: EK(U): " + Utils.bytesToHex(U));
        byte[] sessKey = req.acceptNegotiate(U);
        System.out.println("Req - Final :: Decrypt :: EK(U) (" + sessKey.length + "), U: " + Utils.bytesToHex(sessKey));        
        if (Utils.bytesToHex(resp.getSessionKey()).equals(Utils.bytesToHex(sessKey))) {
            System.out.println("Negotiation of U SUCCEED !");
        } else {
            System.out.println("Key Negotiation of U FAILED !");
        }
    }

    public static byte[] strToBytes(String s) {
        byte[] temp = new byte[s.length()];
        for (int i = 0; i < s.length(); i++) {
            temp[i] = (byte) s.charAt(i);
        }
        return temp;
    }

}
