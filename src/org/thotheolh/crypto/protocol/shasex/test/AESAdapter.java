/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thotheolh.crypto.protocol.shasex.test;

import org.thotheolh.crypto.cipher.CipherAdapter;

/**
 *
 * @author gerald
 */
public class AESAdapter extends CipherAdapter {

    public AESAdapter() {
    }

    @Override
    public byte[] encryptBlock(byte[] key, byte[] data) {
        AES aes = new AES();
        aes.setKey(key);
        return aes.encrypt(data);
    }

    @Override
    public byte[] decryptBlock(byte[] key, byte[] data) {
        AES aes = new AES();
        aes.setKey(key);
        return aes.decrypt(data);
    }

}
