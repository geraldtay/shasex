/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.thotheolh.crypto.protocol.shasex;

import java.security.SecureRandom;
import org.thotheolh.crypto.cipher.CipherAdapter;

/**
 *
 * @author gerald
 */
public class Requester {
    
    CipherAdapter cipher;
    byte[] secret;
    int keyLen = 32; // 256 bit key length as default.
    byte[] R;
    SecureRandom srand = new SecureRandom();
    
    public Requester(CipherAdapter cipher, byte[] secret, int keyByteLength) {
        this.cipher = cipher;
        this.secret = secret;
        if (keyByteLength >= 16) {
            this.keyLen = keyByteLength;
        }
    }

    // Requester must call this first. Generates R.
    public byte[] beginRequest() {
        R = srand.generateSeed(keyLen);
        System.out.println("R (" + R.length + "), " + Utils.bytesToHex(R));
        return cipher.encryptBlock(secret, R);
    }

    // Call this to accept negotiated session key (U).
    public byte[] acceptNegotiate(byte[] responderNegotiateKey) {
        // Returns session key key.
        return cipher.decryptBlock(R, responderNegotiateKey);
    }
    
}
