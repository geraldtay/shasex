/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.thotheolh.crypto.cipher;

/**
 *
 * @author gerald
 */
public abstract class CipherAdapter {
    
    public abstract byte[] encryptBlock(byte[] key, byte[] data);
    
    public abstract byte[] decryptBlock(byte[] key, byte[] data);
    
}
